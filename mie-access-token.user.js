// ==UserScript==
// @name         Access Token
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Click on Kinesso logo in Admin Page to get access token
// @author       Richie Gee
// @include      https://mie.kinesso.com/*
// @include      https://mie.matterkind.com/*
// @icon         https://www.google.com/s2/favicons?domain=kinesso.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
     function myFunction() {
        var auth_token = localStorage.getItem("accessToken")
        navigator.clipboard.writeText(auth_token);
        /* Alert the copied text */
        alert("Copied the text: " + auth_token);
    }
    window.addEventListener('load', function() {
        // https://stackoverflow.com/questions/16704209/onclick-added-with-tampermonkey-is-not-calling-the-function
        // alert("Script started")
        var logo_div = document.querySelector ("#nav-header-logo");
        if (logo_div) {
            logo_div.addEventListener("click", myFunction, false);
        }
    }, false);
})();